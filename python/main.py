# -*- coding: utf-8 -*-
import json
import socket
import sys,math
from bot_util import *


class Bottara(object):

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key

        #World Information
            #A list of dictionaries, each representing a piece of the track. (self.map[0] is the first piece, the start)
        self.map = []
            #Number of pieces in the map.
        self.map_length = 0
            #A list of dictionaries, each representing a lane. self.lane[i]['index'] == i and self.lane[i]['distanceFromCenter'] is the lane's distance from the center of the road.
        self.lanes = []
            #Number of lanes.
        self.num_lanes = 0

        #Position and speed of car
            #ID of the piece of the map the car is on.
        self.car_piece = 0
            #Position of the car on that piece.
        self.car_pos = 0
            #Car speed, calculated each tick.
        self.car_speed = 0
            #The current lane the car is on. (or the starting lane when the car is changing lanes.
        self.car_lane = 0
            #Is the car changing lanes?
        self.car_changing_lane = False
            #The new lane the car is moving to.
        self.car_new_lane = 0
            #The current lap.
        self.car_lap = 0
            #The throttle value.
        self.car_throttle = 0.0
            #The power of the engine.
        self.car_engine = 0.0
            #The angle the car has.
        self.car_angle = 0
        

        #Car dimensions
        self.car_width = 0
        self.car_length = 0
        self.quide_flag = 0
        
        #Is this a quickrace?
        # WHAT THE FUCK DOES THIS EVEN MEAN?
        self.quickrace = False

        #Our car's id
        self.name = "NULL"
        self.color = "red"

        #Helper/Temp
        self.engine_check = False
        self.crash_check = False
        self.once = True
        self.old_speed = 0
        self.counter = 0
        self.engine_counter = 0
        self.engine_time = 0
        self.old_angle = 0.0
        self.degrees_traveled = 0.0

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.send(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    
    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def switch_lane(self, direction):
        ''' direction -> string ('Left' | 'Right') '''
        self.msg("switchLane",direction)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.ping()

    def on_car_positions(self, data):
        self.find_speed(data)

        #self.car_throttle = 0.655
        #if self.once:
        #    self.switch_lane("Right")
        #    self.once = False
        #if self.car_changing_lane and self.counter == 0:
        #    self.counter = self.counter + 1
        #    self.switch_lane("Left")

        #Learn the world
        if self.engine_check:
            self.engine_time = self.engine_time + 1
            self.car_throttle = 0.1
            if self.old_speed > 0.0 and (self.car_speed - self.old_speed) < 0.00001:
                self.engine_counter = self.engine_counter + 1
                if self.engine_counter>3:
                    self.engine_check = False
                    self.car_engine = 10*self.car_speed
                    print "Engine max: " + str(self.car_engine) + " (Found in: " + str(self.engine_time) + ")"
            else:
                self.engine_counter = 0
            self.old_speed = self.car_speed

        elif self.crash_check:
            self.car_throttle = 1.0
            self.find_angle()
                
        #Evaluate circumstances
        else:
            self.car_throttle = 0.655
            #print self.car_speed
            self.old_speed = self.car_speed
            #if self.once:  
            #    self.car_throttle = 0.655
            #else:
            #    self.counter = self.counter + 1
            #if self.car_throttle*self.car_engine - self.car_speed < 0.00001 and self.once:
            #    print "Starting deacceleration from speed: " + str(self.car_speed)
            #    self.car_throttle = 0.000
            #    self.once = False
            #if self.car_speed < 0.01 and not self.once:
            #    print "Ticks to reach <0.01 speed: " + str(self.counter) + " | Actual speed: " + str(self.car_speed)
            #    self.counter = 0
            #    self.once = True


        #Send choices
        self.throttle(self.car_throttle)

    def find_angle(self):
        if 'radius' in self.map[self.car_piece]:
            degrees_added = abs(self.car_angle - self.old_angle)
            distance_traveled = self.car_speed
            circum = self.map[self.car_piece]['radius']*2*math.pi
            self.degrees_traveled = (distance_traveled*360)/circum
            #print str(self.degrees_traveled - degrees_added)
            self.old_angle = self.car_angle
        
    def find_speed(self, data):
        for i in data:
            if i['id']['color'] == self.color:
                self.car_angle = i['angle']
                newpiece = i['piecePosition']['pieceIndex']
                newpos = i['piecePosition']['inPieceDistance']
                self.car_new_lane = i['piecePosition']['lane']['endLaneIndex']
                if self.car_new_lane != self.car_lane:
                    self.car_changing_lane = True
                else:
                    self.car_lane = self.car_new_lane
                    self.car_changing_lane = False
                self.lap = i['piecePosition']['lap']
                if newpiece == self.car_piece:
                    self.car_speed = newpos - self.car_pos
                    self.car_pos = newpos
                else:
                    length = 0
                    if "length" in self.map[self.car_piece]:
                        length = self.map[self.car_piece]['length']
                    else:
                        length = self.curve_length(self.map[self.car_piece],self.car_lane)
                    self.car_speed = length - self.car_pos + newpos
                    self.car_piece = newpiece
                    self.car_pos = newpos
                break   
                
    
    def curve_length(self, piece, laneIndex):
        if piece['angle']>0:
            return (2.0*math.pi*(piece['radius'] - self.lanes[laneIndex]['distanceFromCenter'])*abs(piece['angle']))/360.0
        return (2.0*math.pi*(piece['radius'] + self.lanes[laneIndex]['distanceFromCenter'])*abs(piece['angle']))/360.0
     
    def find_length(self,piece,lane_from,lane_to):
        '''
        For any given piece, return the length the car will
        travel to traverse it
        Piece: the piece we are examining
        lane_from: The lane we currently are. During switch, it is the origin lane
        lane_to: SWITCH ONLY the lane we are heading to
        '''
        
        if 'radius' in piece: # or whatever, you get it
            return self.curve_length(piece,lane_from)
        elif 'switch' in piece and lane_from!=lane_to:
            return math.sqrt(abs(self.lanes[lane_from]['distanceFromCenter']-self.lanes[lane_to]['distanceFromCenter'])**2+piece['length']**2)
        return piece['length']
                
    def length(self,sumS,piece_n,lane):
        '''
        DFS search
        Recursively tests all combinations of lane switches and finds
        the shortest path around the track. Returns a list containing
        - Length: A float, the sum of the chose path
        - Path: A list of the choices that must be made on each switch piece
        sumS: The sum so far.
        piece_n: Index of the piece
        lane: The lane we are currently on
        
        This is the standard or 'classic' racing line concept which relies on
        minimum distance to calculate the optimal path. Keep in mind that using 
        speed or time as the target variable might actually (probably will)
        improve our time around the track. This is just the first step.
        '''

        if piece_n>=self.map_length:
            return [sumS,[]]

        elif 'switch' in self.map[piece_n]:
            minim = self.length(sumS,piece_n+1,lane)
            l = lane
            for i in [-1,1]:
                if i+lane>0 and i+lane<self.num_lanes:
                    temp = self.length(sumS,piece_n+1,i+lane)
                    if temp[0] < minim[0]:
                        minim = temp
                        l = i+lane
            lanes = []
            lanes.append(l)
            for i in minim[1]:
                lanes.append(i)
            
            sumS = minim[0]
            sumS += self.find_length(self.map[piece_n],lane,l)

            return [sumS,lanes]
        else:
            sumS += self.find_length(self.map[piece_n],lane,lane)
            
        return self.length(sumS,piece_n+1,lane)  
        
    def on_crash(self, data):
        # Should also switch profile to set-up/test
        print("Someone crashed")
        if data['color'] == self.color:
            self.car_throttle = 0.0
            self.crash_check = False
            #self.change_lane("crashpls")
        self.ping()

    def on_game_end(self, data):
        print("Race ended")
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def on_your_car(self, data):
        self.name = data['name']
        self.color = data['color']
        self.ping()

    def on_game_init(self, data):
        self.map = data['race']['track']['pieces']
        self.map_length = len(data['race']['track']['pieces'])
        self.lanes = data['race']['track']['lanes']
        self.num_lanes = len(data['race']['track']['lanes'])
        self.car_width = data['race']['cars'][0]['dimensions']['width']
        self.car_length = data['race']['cars'][0]['dimensions']['length']
        self.guide_flag = data['race']['cars'][0]['dimensions']['guideFlagPosition']
        print data['race']['raceSession']
        if 'quickrace' in data['race']['raceSession']:
            self.quickrace = data['race']['raceSession']['quickRace']
        else:
            self.quickrace = False
        self.process_map()
        (total,final) = self.length(0,0,0)
        print total
        print final
        self.ping()

    def process_map(self):
        self.fixed_map = []
        # Why fixxed and not fixed?
        length = 0
        angle = 0
        radius = 0
        isAngle = 'radius' in self.map[0]
        for i in self.map:
            if isAngle:
                if 'radius' in i and radius==i['radius'] and angle*i['angle']>0:
                    angle = angle + i['angle']
                else:
                    self.fixed_map.append(TrackPiece(-1,angle,radius))
                    length = 0
                    angle = 0
                    radius = 0
                    if 'radius' in i:
                        isAngle = True
                        angle = i['angle']
                        radius = i['radius']
                    else:
                        isAngle = False
                        length = i['length']
            else:
                if 'length' in i:
                    length = length + i['length']
                else:
                    self.fixed_map.append(TrackPiece(length,0,-1))
                    length = 0
                    isAngle = True
                    angle = i['angle']
                    radius = i['radius']

        if isAngle:
            self.fixed_map.append(TrackPiece(-1,angle,radius))
        else:
            self.fixed_map.append(TrackPiece(length,0,-1))
            

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'gameInit': self.on_game_init,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'yourCar': self.on_your_car,
            'error': self.on_error,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = Bottara(s, name, key)
        bot.run()
